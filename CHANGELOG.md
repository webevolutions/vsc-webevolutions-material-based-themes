# Change Log

All notable changes to the "webevolutions-material-themes" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

- Initial release

## [0.0.2] - 2021-07-13
- Add more package infos

## [0.0.3] - 2021-07-13
- Add package icon

## [0.0.4] - 2021-07-13
- Change package icon

## [0.0.5] - 2021-07-15
### Added
- color php
- color json

## [0.0.6] - 2021-07-15
### Change
- color php

## [0.0.7] - 2021-07-15
### Add
- color scss

## [0.0.8] - 2021-07-16
### Add
- color ts
